import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from './user';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DataService} from '../data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})


export class UsersComponent implements OnInit, OnDestroy {

  filtersLoaded: Promise<boolean> | undefined;
  users: User[] | undefined;
  columns = ['Name', 'Email', 'Role', 'Actions'];
  query: any;
  collectionSize: number | undefined;
  page = 1;
  pageSize = 3;
  pageSizes = [3, 5, 10];
  addUserForm = new FormGroup({
    email: new FormControl(''),
    first_name: new FormControl(''),
    initials: new FormControl(''),
    last_name: new FormControl(''),
    date_of_birth: new FormControl(''),
    main_study: new FormControl(''),
    student_number: new FormControl(''),
    phone_number: new FormControl(''),
    address: new FormControl(''),
    postal_code: new FormControl(''),
    city: new FormControl(''),
    iban: new FormControl(''),
    teaching_level: new FormControl(''),
    teaching_area: new FormControl(''),
    committee: new FormControl(''),
    member_since: new FormControl(''),
    payment_auth_membership: new FormControl(''),
    payment_auth_activities: new FormControl(''),
    payment_characteristic: new FormControl(''),
    comments: new FormControl(''),
    contribution: new FormControl(''),
    mandate: new FormControl(''),
    used_for: new FormControl(''),
    role: new FormControl('')
  });
  editUserForm = new FormGroup({
    id: new FormControl(''),
    email: new FormControl(''),
    first_name: new FormControl(''),
    initials: new FormControl(''),
    last_name: new FormControl(''),
    date_of_birth: new FormControl(''),
    main_study: new FormControl(''),
    student_number: new FormControl(''),
    phone_number: new FormControl(''),
    address: new FormControl(''),
    postal_code: new FormControl(''),
    city: new FormControl(''),
    iban: new FormControl(''),
    teaching_level: new FormControl(''),
    teaching_area: new FormControl(''),
    committee: new FormControl(''),
    member_since: new FormControl(''),
    payment_auth_membership: new FormControl(''),
    payment_auth_activities: new FormControl(''),
    payment_characteristic: new FormControl(''),
    comments: new FormControl(''),
    contribution: new FormControl(''),
    mandate: new FormControl(''),
    used_for: new FormControl(''),
    role: new FormControl('')
  });
  deleteUserForm = new FormGroup({
    id: new FormControl(''),
    email: new FormControl(''),
    full_name: new FormControl('')
  });

  constructor(public modalService: NgbModal, private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getAllUsers().subscribe(data => {
      this.users = data;
      this.collectionSize = data.length;
      this.dataService.filtersLoaded.next(true);
      this.filtersLoaded = Promise.resolve(true);
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  onSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
  }

  openEditModal(targetModal: any, user: User): void {
    this.editUserForm.patchValue({
      id: user.id,
      email: user.email,
      first_name: user.first_name,
      initials: user.initials,
      last_name: user.last_name,
      date_of_birth: user.date_of_birth,
      main_study: user.main_study,
      student_number: user.student_number,
      phone_number: user.phone_number,
      address: user.address,
      postal_code: user.postal_code,
      city: user.city,
      iban: user.iban,
      teaching_level: user.teaching_level,
      teaching_area: user.teaching_area,
      committee: user.committee?.split(','),
      member_since: user.member_since,
      payment_auth_membership: user.payment_auth_membership,
      payment_auth_activities: user.payment_auth_activities,
      payment_characteristic: user.payment_characteristic,
      comments: user.comments,
      contribution: user.contribution,
      mandate: user.mandate,
      used_for: user.used_for,
      role: user.role
    });
    console.log(this.editUserForm.get('committee')?.value);
    this.modalService.open(targetModal, {size: 'lg'});
  }

  openDeleteModal(targetModal: any, user: User): void {
    this.deleteUserForm.patchValue({
      id: user.id,
      email: user.email,
      full_name: user.full_name
    });
    this.modalService.open(targetModal);
  }

  onSubmitAdd(): void {
    this.dataService.sendRequestAddUser(this.addUserForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitEdit(): void {
    console.log(this.editUserForm.get('committee')?.value);
    this.dataService.sendRequestEditAsAdmin(this.editUserForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitDelete(): void {
    this.dataService.sendRequestDeleteUser(this.deleteUserForm.get('id')?.value).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }
}
