import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {DataService} from '../data.service';
import {User} from '../users/user';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  filtersLoaded: Promise<boolean> | undefined;
  user: User | undefined;
  userForm = new FormGroup({
    email: new FormControl(''),
    first_name: new FormControl(''),
    initials: new FormControl(''),
    last_name: new FormControl(''),
    date_of_birth: new FormControl(''),
    phone_number: new FormControl(''),
    student_number: new FormControl(''),
    main_study: new FormControl(''),

  });
  changePasswordForm = new FormGroup({
    password: new FormControl(''),
    new_password: new FormControl(''),
    confirm: new FormControl(''),
  });

  constructor(public modalService: NgbModal, private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getGlobalUser.subscribe(data => {
      this.user = data;
      this.userForm.patchValue({
        email: this.user?.email,
        first_name: this.user?.first_name,
        initials: this.user?.initials,
        last_name: this.user?.last_name,
        date_of_birth: this.user?.date_of_birth,
        phone_number: this.user?.phone_number,
        student_number: this.user?.student_number,
        main_study: this.user?.main_study,
      });
      this.filtersLoaded = Promise.resolve(true);
      this.dataService.filtersLoaded.next(true);
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  onSubmitEdit(): void {
    this.dataService.sendRequestEditAsUser(this.userForm).subscribe((response) => {
      location.reload();
    });
  }

  onSubmitPassword(): void {
    this.dataService.sendRequestChangePassword(this.changePasswordForm).subscribe((response) => {
      this.modalService.dismissAll();
    });
  }
}
