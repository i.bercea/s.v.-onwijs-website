import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {User} from './users/user';
import {BehaviorSubject, Observable} from 'rxjs';
import {FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {Announcement} from './announcements/announcement';
import {Vacancy} from './vacancies/vacancy';
import {Activity} from './committees/activity';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  rootURL = `http://127.0.0.1:5000/`;
  headers: HttpHeaders = new HttpHeaders({
    'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
  });
  globalUser = new BehaviorSubject<any>(User);
  getGlobalUser = this.globalUser.asObservable();
  filtersLoaded = new BehaviorSubject<any>(false);
  getFiltersLoaded = this.filtersLoaded.asObservable();
  private accessToken: string | null | undefined;

  constructor(private http: HttpClient) {
  }

  createAuthorizationHeader(): void {
    this.accessToken = localStorage.getItem('access_token');
    if (this.accessToken) {
      this.headers = this.headers.set('Authorization', `Bearer ${this.accessToken}`);
    }
  }

  getUser(): Observable<User> {
    this.createAuthorizationHeader();
    return this.http.get<User>(this.rootURL + `user`, {
      headers: this.headers,
    });
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.rootURL + `users`, {
      headers: this.headers,
    });
  }

  getUsersByCommittee(committee: string): Observable<User[]> {
    return this.http.get<User[]>(this.rootURL + `users/${committee}`);
  }

  getActivities(committee: string): Observable<Activity[]> {
    return this.http.get<Activity[]>(this.rootURL + `activities/${committee}`);
  }

  getAllVacancies(): Observable<Vacancy[]> {
    return this.http.get<Vacancy[]>(this.rootURL + `vacancies`);
  }

  getAllAnnouncements(): Observable<Announcement[]> {
    return this.http.get<Announcement[]>(this.rootURL + `announcements`);
  }

  getAllEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(this.rootURL + `events`);
  }

  sendContactForm(form: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`name`, form.get('name')?.value)
      .set(`email`, form.get('email')?.value)
      .set(`message`, form.get('message')?.value)
      .toString();

    return this.http.post(this.rootURL + `contact`, params, {
      headers: this.headers,
      observe: 'response'
    });
  }

  sendSignUpForm(form: FormGroup): Observable<HttpResponse<any>> {
    let params = new HttpParams();
    switch (form.get('contribution')?.value) {
      case '1': {
        params = params.set(`contribution`, '4.25')
          .set(`program`, form.get('minorProgram')?.value);
        break;
      }
      case '2': {
        params = params.set(`contribution`, '8.5')
          .set(`program`, form.get('masterProgram')?.value);
        break;
      }
      default: {
        params = params.set(`contribution`, '4.25')
          .set(`program`, '');
        break;
      }
    }

    if (form.get('paymentMembership')?.value) {
      params = params.set(`payment_membership`, 'yes');
    } else {
      params = params.set(`payment_membership`, 'no');
    }

    if (form.get('paymentActivities')?.value) {
      params = params.set(`payment_activities`, 'yes');
    } else {
      params = params.set(`payment_activities`, 'no');
    }

    params = params.set(`first_name`, form.get('firstName')?.value)
      .set(`initials`, form.get('initials')?.value)
      .set(`last_name`, form.get('lastName')?.value)
      .set(`date_of_birth`, form.get('dateOfBirth')?.value)
      .set(`main_study`, form.get('study')?.value)
      .set(`startYear`, form.get('startYear')?.value)
      .set(`student_number`, form.get('sNumber')?.value)
      .set(`street_address`, form.get('streetAddress')?.value)
      .set(`city`, form.get('city')?.value)
      .set(`postal_code`, form.get('postalCode')?.value)
      .set(`phone_number`, form.get('telephone')?.value)
      .set(`email`, form.get('email')?.value)
      .set(`iban`, form.get('iban')?.value);

    return this.http.post(this.rootURL + `signup`, params.toString(), {
      headers: this.headers,
      observe: 'response'
    });
  }

  sendRequestLogin(user: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`email`, user.get('email')?.value)
      .set(`password`, user.get('password')?.value)
      .toString();

    return this.http.post(this.rootURL + `login`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestLogout(): Observable<any> {
    return this.http.delete(this.rootURL + `logout`, {
      headers: this.headers,
    });
  }

  sendRequestChangePassword(user: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`password`, user.get('password')?.value)
      .set(`new_password`, user.get('new_password')?.value)
      .set(`confirm`, user.get('confirm')?.value)
      .toString();

    return this.http.post(this.rootURL + `change-password`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestGetToken(emailForm: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams().set(`email`, emailForm.get('email')?.value).toString();

    return this.http.post(this.rootURL + `reset-password`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestResetPassword(passwordResetForm: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`password`, passwordResetForm.get('newPassword')?.value)
      .set(`token`, passwordResetForm.get('token')?.value)
      .toString();

    return this.http.post(this.rootURL + `reset-password-confirmed`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestAddUser(user: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`email`, user.get('email')?.value)
      .set(`first_name`, user.get('first_name')?.value)
      .set(`initials`, user.get('initials')?.value)
      .set(`last_name`, user.get('last_name')?.value)
      .set(`date_of_birth`, user.get('date_of_birth')?.value)
      .set(`main_study`, user.get('main_study')?.value)
      .set(`student_number`, user.get('student_number')?.value)
      .set(`phone_number`, user.get('phone_number')?.value)
      .set(`address`, user.get('address')?.value)
      .set(`postal_code`, user.get('postal_code')?.value)
      .set(`city`, user.get('city')?.value)
      .set(`iban`, user.get('iban')?.value)
      .set(`teaching_level`, user.get('teaching_level')?.value)
      .set(`teaching_area`, user.get('teaching_area')?.value)
      .set(`committee`, user.get('committee')?.value)
      .set(`member_since`, user.get('member_since')?.value)
      .set(`payment_auth_membership`, user.get('payment_auth_membership')?.value)
      .set(`payment_auth_activities`, user.get('payment_auth_activities')?.value)
      .set(`payment_characteristic`, user.get('payment_characteristic')?.value)
      .set(`comments`, user.get('comments')?.value)
      .set(`contribution`, user.get('contribution')?.value)
      .set(`mandate`, user.get('mandate')?.value)
      .set(`used_for`, `RCUR`)
      .set(`role`, user.get('role')?.value)
      .toString();

    return this.http.post(this.rootURL + `users/add`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestEditAsUser(user: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`email`, user.get('email')?.value)
      .set(`first_name`, user.get('first_name')?.value)
      .set(`initials`, user.get('initials')?.value)
      .set(`last_name`, user.get('last_name')?.value)
      .set(`date_of_birth`, user.get('date_of_birth')?.value)
      .set(`main_study`, user.get('main_study')?.value)
      .set(`student_number`, user.get('student_number')?.value)
      .toString();

    return this.http.post(this.rootURL + `users/edit`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestEditAsAdmin(user: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`email`, user.get('email')?.value)
      .set(`first_name`, user.get('first_name')?.value)
      .set(`initials`, user.get('initials')?.value)
      .set(`last_name`, user.get('last_name')?.value)
      .set(`date_of_birth`, user.get('date_of_birth')?.value)
      .set(`main_study`, user.get('main_study')?.value)
      .set(`student_number`, user.get('student_number')?.value)
      .set(`phone_number`, user.get('phone_number')?.value)
      .set(`address`, user.get('address')?.value)
      .set(`postal_code`, user.get('postal_code')?.value)
      .set(`city`, user.get('city')?.value)
      .set(`iban`, user.get('iban')?.value)
      .set(`teaching_level`, user.get('teaching_level')?.value)
      .set(`teaching_area`, user.get('teaching_area')?.value)
      .set(`committee`, user.get('committee')?.value)
      .set(`member_since`, user.get('member_since')?.value)
      .set(`payment_auth_membership`, user.get('payment_auth_membership')?.value)
      .set(`payment_auth_activities`, user.get('payment_auth_activities')?.value)
      .set(`payment_characteristic`, user.get('payment_characteristic')?.value)
      .set(`comments`, user.get('comments')?.value)
      .set(`contribution`, user.get('contribution')?.value)
      .set(`mandate`, user.get('mandate')?.value)
      .set(`used_for`, `RCUR`)
      .set(`role`, user.get('role')?.value)
      .toString();

    return this.http.post(this.rootURL + `users/edit/${user.get('id')?.value}`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestDeleteUser(id: number): Observable<any> {
    return this.http.delete(this.rootURL + `users/delete/${id}`, {
      headers: this.headers
    });
  }

  sendRequestAddEvent(event: FormGroup): Observable<HttpResponse<any>> {
    this.createAuthorizationHeader();
    let params = new HttpParams()
      .set(`title`, event.get('title')?.value)
      .set(`start`, moment(event.get('start')?.value).format('YYYY-MM-DD HH:mm:00'))
      .set(`backgroundColor`, event.get('backgroundColor')?.value);

    if (event.get('end')?.value !== '') {
      params = params.set(`end`, moment(event.get('end')?.value).format('YYYY-MM-DD HH:mm:00')).set(`allDay`, String(0));
    } else {
      params = params.set(`end`, '').set(`allDay`, String(1));
    }

    return this.http.post(this.rootURL + `events/add`, params.toString(), {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestEditEvent(event: FormGroup): Observable<HttpResponse<any>> {
    let params = new HttpParams()
      .set(`title`, event.get('title')?.value)
      .set(`start`, moment(event.get('start')?.value).format('YYYY-MM-DD HH:mm:00'))
      .set(`backgroundColor`, event.get('backgroundColor')?.value);

    if (event.get('end')?.value) {
      params = params.set(`end`, moment(event.get('end')?.value).format('YYYY-MM-DD HH:mm:00')).set(`allDay`, String(0));
    } else {
      params = params.set(`end`, '').set(`allDay`, String(1));
    }
    return this.http.post(this.rootURL + `events/edit/${event.get('id')?.value}`, params.toString(), {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestDeleteEvent(id: number): Observable<HttpResponse<any>> {
    return this.http.delete(this.rootURL + `events/delete/${id}`, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestAddAnnouncement(announcement: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, announcement.get('title')?.value)
      .set(`content`, announcement.get('content')?.value)
      .set(`created_at`, moment(Date.now()).format('YYYY-MM-DD'))
      .toString();
    return this.http.post(this.rootURL + `announcements/add`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestEditAnnouncement(announcement: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, announcement.get('title')?.value)
      .set(`content`, announcement.get('content')?.value)
      .set(`created_at`, announcement.get('created_at')?.value)
      .toString();
    return this.http.post(this.rootURL + `announcements/edit/${announcement.get('id')?.value}`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestDeleteAnnouncement(id: number): Observable<any> {
    return this.http.delete(this.rootURL + `announcements/delete/${id}`, {
      headers: this.headers
    });
  }

  sendRequestAddVacancy(vacancy: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, vacancy.get('title')?.value)
      .set(`content`, vacancy.get('content')?.value)
      .set(`created_at`, moment(Date.now()).format('YYYY-MM-DD'))
      .toString();

    return this.http.post(this.rootURL + `vacancies/add`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestEditVacancy(vacancy: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, vacancy.get('title')?.value)
      .set(`content`, vacancy.get('content')?.value)
      .set(`created_at`, vacancy.get('created_at')?.value)
      .toString();

    return this.http.post(this.rootURL + `vacancies/edit/${vacancy.get('id')?.value}`, params, {
      headers: this.headers,
      observe: 'response',
    });
  }

  sendRequestDeleteVacancy(id: number): Observable<any> {
    return this.http.delete(this.rootURL + `vacancies/delete/${id}`, {
      headers: this.headers
    });
  }

  sendImages(images: File[], committee: string, id: number): Observable<any> {
    const formData: FormData = new FormData();
    for (const image of images) {
      formData.append('file', image);
    }
    return this.http.post(this.rootURL + `activities/images/${committee}/${id}`, formData, {
      reportProgress: true
    });
  }

  getImages(committee: string, id: number): Observable<HttpResponse<any>> {
    return this.http.get(this.rootURL + `activities/images/${committee}/${id}`, {
      observe: 'response'
    });
  }

  deleteImages(committee: string, id: number): Observable<any> {
    return this.http.delete(this.rootURL + `activities/images/${committee}/${id}`, {
      headers: this.headers
    });
  }

  sendRequestAddActivity(activity: FormGroup, committee: string): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, activity.get('title')?.value)
      .set(`content`, activity.get('content')?.value)
      .set(`created_at`, moment(Date.now()).format('YYYY-MM-DD'))
      .toString();

    return this.http.post(this.rootURL + `activities/${committee}/add`, params, {
      headers: this.headers,
      observe: 'response'
    });
  }

  sendRequestEditActivity(activity: FormGroup): Observable<HttpResponse<any>> {
    const params = new HttpParams()
      .set(`title`, activity.get('title')?.value)
      .set(`content`, activity.get('content')?.value)
      .set(`created_at`, activity.get('created_at')?.value)
      .toString();

    return this.http.post(this.rootURL + `activities/edit/${activity.get('id')?.value}`, params, {
      headers: this.headers,
      observe: 'response'
    });
  }

  sendRequestDeleteActivity(id: number): Observable<any> {
    return this.http.delete(this.rootURL + `activities/delete/${id}`, {
      headers: this.headers
    });
  }
}
