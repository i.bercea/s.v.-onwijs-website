import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {DataService} from '../data.service';
import {Announcement} from '../announcements/announcement';
import {User} from '../users/user';
import {CalendarOptions} from '@fullcalendar/angular';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import * as moment from 'moment';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {Router} from '@angular/router';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {
  @ViewChild('eventModal') eventModal: any;

  constructor(private router: Router, public dataService: DataService, private modalService: NgbModal) {
  }

  user: User | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  announcements: Announcement[] | undefined;
  calendarOptions: CalendarOptions | undefined;
  editorConfig: AngularEditorConfig = {
    editable: false,
    sanitize: true,
    outline: false,
    enableToolbar: false,
    showToolbar: false,
  };
  eventForm = new FormGroup({
    title: new FormControl(''),
    start: new FormControl(''),
    end: new FormControl(''),
  });
  announcementForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    created_at: new FormControl(''),
    created_by: new FormControl(''),
  });

  ngOnInit(): void {
    this.dataService.getAllAnnouncements().subscribe(data1 => {
      this.announcements = data1;
      this.dataService.getAllEvents().subscribe(data2 => {
        this.calendarOptions = {
          plugins: [dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin],
          initialView: 'dayGridMonth',
          events: data2,
          dayMaxEvents: true,
          businessHours: true,
          displayEventEnd: true,
          weekends: true,
          height: 'auto',
          themeSystem: 'bootstrap',
          headerToolbar: {
            left: 'title',
            right: 'today prev,next timeGridDay,timeGridWeek,dayGridMonth,listWeek'
          },
          eventClick: arg => {
            this.eventForm.patchValue({
              title: arg.event.title,
              start: moment(arg.event.start).format('MMMM d yyyy hh:mm a'),
            });
            arg.event.end ? this.eventForm.patchValue({
              end: moment(arg.event.end).format('MMMM d yyyy hh:mm a'),
            }) : this.eventForm.patchValue({
              end: 'All-day event',
            });
            this.modalService.open(this.eventModal);
          }
        };
        if (localStorage.getItem('access_token')) {
          this.dataService.getGlobalUser.subscribe(data3 => {
            this.user = data3;
            this.filtersLoaded = Promise.resolve(true);
            this.dataService.filtersLoaded.next(true);
          });
        } else {
          this.filtersLoaded = Promise.resolve(true);
          this.dataService.filtersLoaded.next(true);
        }
      });
    }, () => {
      localStorage.removeItem('access_token');
      this.router.navigate(['/home']);
      this.user = undefined;
      this.dataService.globalUser.next(null);
      this.dataService.getFiltersLoaded.subscribe(loaded => this.filtersLoaded = Promise.resolve(loaded));
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  openAnnouncementModal(targetModal: any, announcement: Announcement): void {
    this.announcementForm.patchValue({
      title: announcement.title,
      content: announcement.content,
      created_at: announcement.created_at,
      created_by: announcement.created_by,
    });
    this.modalService.open(targetModal, {size: 'lg'});
  }

  // onTokenRequest() {
  //   let postData: JSON = JSON.parse('{"email": "' + this.email.value + '"}');
  //
  //   this.dataService.sendPostRequest('reset-password', postData).subscribe((data: any) => {
  //     if (data.status == 201) {
  //       alert("An email has been sent to your email address. Once you receive your token, please continue to the next page.");
  //     } else {
  //       alert("An error has been encountered. Please, try again later!");
  //     }
  //   });
  // }

  // onPasswordReset() {
  //   if (this.verpassword !== this.newpassword) {
  //     alert("Password do not match.");
  //   } else {
  //     let postData: JSON = JSON.parse("{'password': '" + this.newpassword + "', 'confirm': '" + this.verpassword + "'}")
  //     this.dataService.sendPostRequest('reset-password/' + this.token, postData).subscribe((data: any) => {
  //       if (data.status == 200) {
  //         alert("Your password has been reset successfully! You may now login.");
  //       } else {
  //         alert("An error has been encountered. Please, try again later!");
  //       }
  //     });
  //   }
  // }
}
