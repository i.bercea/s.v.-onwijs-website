import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {UsersComponent} from './users/users.component';
import {ProfileComponent} from './profile/profile.component';
import {AnnouncementsComponent} from './announcements/announcements.component';
import {EventsComponent} from './events/events.component';
import {AboutComponent} from './about/about.component';
import {VacanciesComponent} from './vacancies/vacancies.component';
import {CommitteesComponent} from './committees/committees.component';

const routes: Routes = [
  {path: 'home', component: IndexComponent},
  {path: 'users', component: UsersComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'announcements', component: AnnouncementsComponent},
  {path: 'events', component: EventsComponent},
  {path: 'about', component: AboutComponent},
  {path: 'vacancies', component: VacanciesComponent},
  {
    path: 'committees',
    children: [
      {path: 'kfc', component: CommitteesComponent},
      {path: 'olc', component: CommitteesComponent},
      {path: 'kascommissie', component: CommitteesComponent},
      {path: 'raadvanadvies', component: CommitteesComponent},
      {path: 'oproepcie', component: CommitteesComponent},
    ]
  },
  {path: '**', component: IndexComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
