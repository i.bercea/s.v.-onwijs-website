import {Component, OnInit} from '@angular/core';
import {DataService} from './data.service';
import {User} from './users/user';
import {Announcement} from './announcements/announcement';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'client';
  credentials = true;
  user: User | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  announcements: Announcement[] | undefined;
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  contactForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
    message: new FormControl('')
  });
  signUpForm = new FormGroup({
    firstName: new FormControl(''),
    initials: new FormControl(''),
    lastName: new FormControl(''),
    dateOfBirth: new FormControl(''),
    study: new FormControl(''),
    startYear: new FormControl(''),
    sNumber: new FormControl(''),
    streetAddress: new FormControl(''),
    city: new FormControl(''),
    postalCode: new FormControl(''),
    telephone: new FormControl(''),
    email: new FormControl(''),
    iban: new FormControl(''),
    contribution: new FormControl(''),
    minorProgram: new FormControl(''),
    masterProgram: new FormControl(''),
    paymentMembership: new FormControl(''),
    paymentActivities: new FormControl('')
  });
  passwordResetForm = new FormGroup({
    newPassword: new FormControl(''),
    verifyPassword: new FormControl(''),
    token: new FormControl('')
  });
  requestTokenForm = new FormGroup({
    email: new FormControl(''),
  });

  constructor(private router: Router, private dataService: DataService, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.dataService.getUser()?.subscribe(user => {
      this.dataService.globalUser.next(user);
      this.dataService.getGlobalUser.subscribe(arg => this.user = arg);
      this.dataService.getFiltersLoaded.subscribe(loaded => this.filtersLoaded = Promise.resolve(loaded));
    }, () => {
      localStorage.removeItem('access_token');
      this.user = undefined;
      this.dataService.globalUser.next(null);
      this.dataService.getFiltersLoaded.subscribe(loaded => this.filtersLoaded = Promise.resolve(loaded));
    });
  }

  onSubmitLogin(): void {
    this.dataService.sendRequestLogin(this.loginForm).subscribe(data => {
        localStorage.setItem('access_token', data.body.access_token);
        this.credentials = true;
        location.reload();
      }, () => {
        this.credentials = false;
        this.dataService.globalUser.next(false);
      }
    );
  }

  onLogout(): void {
    this.dataService.sendRequestLogout().subscribe(() => {
      localStorage.removeItem('access_token');
      this.user = undefined;
      this.dataService.globalUser.next(false);
      location.reload();
    });
  }

  onSubmitContact(): void {
    this.dataService.sendContactForm(this.contactForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitSignUp(event: any): void {
    const form = document.getElementsByClassName('needs-validation')[0] as HTMLFormElement;
    if (!form.checkValidity()) {
      event.preventDefault();
      event.stopPropagation();
      form.classList.add('was-validated');
    } else {
      this.dataService.sendSignUpForm(this.signUpForm).subscribe(() => {
        this.modalService.dismissAll();
        location.reload();
      });
    }
  }

  onTokenRequest(): void {
    this.dataService.sendRequestGetToken(this.requestTokenForm).subscribe();
  }

  onPasswordReset(): void {
    if (this.passwordResetForm.get('newPassword')?.value === this.passwordResetForm.get('verifyPassword')?.value) {
      this.dataService.sendRequestResetPassword(this.passwordResetForm).subscribe(data => {
        localStorage.setItem('access_token', data.body.access_token);
        this.modalService.dismissAll();
        location.reload();
      });
    }
  }

  checkboxChange1(event: any): void {
    this.signUpForm.patchValue({
      paymentMembership: event.target.checked
    });
  }

  checkboxChange2(event: any): void {
    this.signUpForm.patchValue({
      paymentActivities: event.target.checked
    });
  }
}
