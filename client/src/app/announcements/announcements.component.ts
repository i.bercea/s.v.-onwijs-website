import {Component, OnDestroy, OnInit} from '@angular/core';
import {Announcement} from './announcement';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DataService} from '../data.service';
import {FormControl, FormGroup} from '@angular/forms';
import {User} from '../users/user';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.css']
})
export class AnnouncementsComponent implements OnInit, OnDestroy {

  editorConfig: AngularEditorConfig = {
    editable: false,
    sanitize: true,
    outline: false,
    enableToolbar: false,
    showToolbar: false,
  };
  editorConfig2: AngularEditorConfig = {
    editable: true,
    sanitize: true,
    outline: false,
    minHeight: '50vh',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter announcement content...',
    toolbarPosition: 'top',
    toolbarHiddenButtons: [[
      'insertImage',
      'insertVideo',
    ]]
  };
  user: User | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  announcements: Announcement[] | undefined;
  addAnnouncementForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
  });
  editAnnouncementForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
    content: new FormControl(''),
    created_at: new FormControl(''),
    created_by: new FormControl(''),
  });
  deleteAnnouncementForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
  });

  constructor(public modalService: NgbModal, private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getAllAnnouncements().subscribe(data1 => {
      this.announcements = data1;
      this.dataService.getGlobalUser.subscribe(data2 => {
        this.user = data2;
        this.filtersLoaded = Promise.resolve(true);
        this.dataService.filtersLoaded.next(true);
      }, () => {
        localStorage.removeItem('access_token');
        this.user = undefined;
        this.dataService.globalUser.next(null);
        this.filtersLoaded = Promise.resolve(true);
        this.dataService.filtersLoaded.next(true);
      });
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  openEditModal(targetModal: any, announcement: Announcement): void {
    this.editAnnouncementForm.patchValue({
      id: announcement.id,
      title: announcement.title,
      content: announcement.content,
      created_at: announcement.created_at,
      created_by: announcement.created_by,
    });
    this.modalService.open(targetModal, {size: 'lg'});
  }

  openDeleteModal(targetModal: any, announcement: Announcement): void {
    this.deleteAnnouncementForm.patchValue({
      id: announcement.id,
      title: announcement.title,
    });
    this.modalService.open(targetModal);
  }

  onSubmitAdd(): void {
    this.dataService.sendRequestAddAnnouncement(this.addAnnouncementForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitEdit(): void {
    this.dataService.sendRequestEditAnnouncement(this.editAnnouncementForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitDelete(): void {
    this.dataService.sendRequestDeleteAnnouncement(this.deleteAnnouncementForm.get('id')?.value).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }
}

