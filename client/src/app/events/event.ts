export class Event {
  id: number | undefined;
  title: string | undefined;
  start: Date | undefined;
  end: Date | undefined;
  allDay: boolean | undefined;
  backgroundColor: string | undefined;
}
