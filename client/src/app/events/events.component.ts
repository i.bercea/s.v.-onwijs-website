import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CalendarOptions} from '@fullcalendar/core';
import {DataService} from '../data.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit, OnDestroy {
  @ViewChild('addEventModal') addModal: any;
  @ViewChild('editEventModal') editModal: any;

  constructor(private router: Router, private dataService: DataService, private modalService: NgbModal) {
  }

  events: Event[] | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  calendarOptions: CalendarOptions | undefined;
  addEventForm = new FormGroup({
    title: new FormControl(''),
    start: new FormControl(''),
    end: new FormControl(''),
    backgroundColor: new FormControl('#000000'),
  });
  editEventForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
    start: new FormControl(''),
    end: new FormControl(''),
    backgroundColor: new FormControl(''),
  });

  ngOnInit(): void {
    this.dataService.getAllEvents().subscribe(data => {
      this.events = data;
      this.calendarOptions = {
        plugins: [dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin],
        initialDate: Date.now(),
        initialView: 'dayGridMonth',
        customButtons: {
          add: {
            text: 'Add event',
            click: () => {
              this.addEventForm.patchValue({
                start: Date.now(),
              });
              this.modalService.open(this.addModal);
            }
          }
        },
        headerToolbar: {
          left: 'prev,next add',
          center: 'title',
          right: 'timeGridDay,timeGridWeek,dayGridMonth,listWeek'
        },
        events: data,
        dayMaxEvents: true,
        dateClick: () => {
          this.addEventForm.patchValue({
            start: Date.now(),
          });
          this.modalService.open(this.addModal);
        },
        eventClick: arg => {
          this.editEventForm.patchValue({
            id: arg.event.id,
            title: arg.event.title,
            start: arg.event.start,
            allDay: arg.event.allDay,
            backgroundColor: arg.event.backgroundColor,
          });
          this.modalService.open(this.editModal);
        },
        nextDayThreshold: '06:00:00',
        displayEventEnd: true,
        weekends: true,
      };
      this.dataService.filtersLoaded.next(true);
      this.filtersLoaded = Promise.resolve(true);
    }, () => this.router.navigate(['/home']));
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  onSubmitAddEvent(): void {
    this.dataService.sendRequestAddEvent(this.addEventForm)?.subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitEditEvent(): void {
    this.dataService.sendRequestEditEvent(this.editEventForm)?.subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitDeleteEvent(id: number): void {
    this.dataService.sendRequestDeleteEvent(id)?.subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }
}
