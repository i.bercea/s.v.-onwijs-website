import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {IndexComponent} from './index/index.component';
import {UsersComponent} from './users/users.component';
import {DataService} from './data.service';
import {FilterPipe} from './users/pipe';
import {SortDirective} from './directive/sort.directive';
import {ProfileComponent} from './profile/profile.component';
import {AnnouncementsComponent} from './announcements/announcements.component';
import {EventsComponent} from './events/events.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FullCalendarModule} from '@fullcalendar/angular';
import {NgBootstrapDatetimeAngularModule} from 'ng-bootstrap-datetime-angular';
import {AboutComponent} from './about/about.component';
import {VacanciesComponent} from './vacancies/vacancies.component';
import {AngularEditorModule} from '@kolkov/angular-editor';
import {CommitteesComponent} from './committees/committees.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    UsersComponent,
    ProfileComponent,
    AnnouncementsComponent,
    EventsComponent,
    AboutComponent,
    SortDirective,
    FilterPipe,
    VacanciesComponent,
    CommitteesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    FullCalendarModule,
    NgBootstrapDatetimeAngularModule,
    AngularEditorModule,
  ],
  providers: [
    DataService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
