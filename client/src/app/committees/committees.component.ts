import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {User} from '../users/user';
import {Activity} from './activity';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-committees',
  templateUrl: './committees.component.html',
  styleUrls: ['./comittees.component.css']
})
export class CommitteesComponent implements OnInit, OnDestroy {

  loaded = false;
  images: SafeUrl[] = [];
  files: any = [];
  users: User[] | undefined;
  currentUser: User | undefined;
  url = this.route.snapshot.url[0].path;
  activities: Activity[] | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  committee: { [key: string]: any } = {
    'kfc': {
      name: 'KFC',
      description: 'KFC staat voor Koffie Faciliterende Commissie. Deze commissie drinkt koffie, praat over koffie en nog belangrijker; ze halen koffie. Dankzij deze kanjers kun jij elke donderdag genieten van een onwijs lekkere bak koffie.'
    },
    'olc': {name: 'OLC', description: 'OLC description'},
    'kascommissie': {name: 'Kascommissie', description: 'Kascommissie description'},
    'raadvanadvies': {name: 'Raad van Advies', description: 'Raad van Advies description'},
    'oproepcie': {name: 'OproepCie', description: 'De OproepCie bestaat uit al onze lieve leden die niet veel tijd over hebben, maar wel graag hun steentje bij willen dragen. Ze helpen het bestuur met verschillende kleine klusjes. '}
  };
  addActivityForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    images: new FormControl('')
  });
  editActivityForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
    content: new FormControl(''),
    created_at: new FormControl('')
  });
  viewActivityForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
    content: new FormControl(''),
    created_at: new FormControl('')
  });
  deleteActivityForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl('')
  });
  editorConfig: AngularEditorConfig = {
    editable: false,
    sanitize: true,
    outline: false,
    enableToolbar: false,
    showToolbar: false,
  };
  editorConfig2: AngularEditorConfig = {
    editable: true,
    sanitize: true,
    outline: false,
    minHeight: '50vh',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter announcement content...',
    toolbarPosition: 'top',
    toolbarHiddenButtons: [[
      'insertImage',
      'insertVideo',
    ]]
  };

  constructor(private domSanitizer: DomSanitizer, private dataService: DataService,
              private route: ActivatedRoute, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.dataService.getActivities(this.committee[this.url].name).subscribe(data1 => {
      this.activities = data1;
      this.dataService.getUsersByCommittee(this.committee[this.url].name).subscribe(data2 => {
        this.users = data2;
        this.dataService.getGlobalUser.subscribe(data3 => {
          this.currentUser = data3;
          this.filtersLoaded = Promise.resolve(true);
          this.dataService.filtersLoaded.next(true);
        });
      });
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  openEditModal(targetModal: any, activity: Activity): void {
    this.editActivityForm.patchValue({
      id: activity.id,
      title: activity.title,
      content: activity.content,
      created_at: activity.created_at
    });
    this.modalService.open(targetModal, {size: 'lg'});
  }

  openViewModal(targetModal: any, activity: Activity): void {
    this.viewActivityForm.patchValue({
      id: activity.id,
      title: activity.title,
      content: activity.content,
      created_at: activity.created_at
    });
    this.dataService.getImages(this.url, this.viewActivityForm.get('id')?.value).pipe(map((images: any) =>
      images.body.result.map((image: any) => this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;png;base64,' + image))))
      .subscribe(data => {
        this.images = data;
        this.loaded = true;
      });
    this.modalService.open(targetModal, {size: 'xl'});
    this.loaded = false;
  }

  openDeleteModal(targetModal: any, activity: Activity): void {
    this.deleteActivityForm.patchValue({
      id: activity.id,
      title: activity.title
    });
    this.modalService.open(targetModal);
  }

  uploadImages(event: any): void {
    this.files = event.target.files;
  }

  onSubmitAdd(): void {
    this.dataService.sendRequestAddActivity(this.addActivityForm, this.committee[this.url].name).subscribe(data => {
      this.dataService.sendImages(this.files, this.url, data.body.id).subscribe(() => {
        this.modalService.dismissAll();
        location.reload();
      });
    });
  }

  onSubmitEdit(): void {
    this.dataService.sendRequestEditActivity(this.editActivityForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitDelete(): void {
    this.dataService.sendRequestDeleteActivity(this.deleteActivityForm.get('id')?.value).subscribe(() => {
      this.dataService.deleteImages(this.url, this.deleteActivityForm.get('id')?.value).subscribe(() => {
        this.modalService.dismissAll();
        location.reload();
      });
    });
  }
}
