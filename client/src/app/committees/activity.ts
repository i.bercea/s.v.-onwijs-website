export class Activity {
  id: number | undefined;
  title: string | undefined;
  content: string | undefined;
  created_at: Date | undefined;
}
