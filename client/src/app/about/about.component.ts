import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit, OnDestroy {

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.filtersLoaded.next(true);
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }
}
