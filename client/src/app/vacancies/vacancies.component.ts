import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Vacancy} from './vacancy';
import {User} from '../users/user';
import {FormControl, FormGroup} from '@angular/forms';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-vacancies',
  templateUrl: './vacancies.component.html',
  styleUrls: ['./vacancies.component.css']
})
export class VacanciesComponent implements OnInit, OnDestroy {

  editorConfig: AngularEditorConfig = {
    editable: false,
    sanitize: true,
    outline: false,
    enableToolbar: false,
    showToolbar: false,
  };
  editorConfig2: AngularEditorConfig = {
    editable: true,
    sanitize: true,
    outline: false,
    minHeight: '50vh',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter announcement content...',
    toolbarPosition: 'top',
    toolbarHiddenButtons: [[
      'insertImage',
      'insertVideo',
    ]]
  };
  user: User | undefined;
  vacancies: Vacancy[] | undefined;
  filtersLoaded: Promise<boolean> | undefined;
  addVacancyForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
  });
  editVacancyForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
    content: new FormControl(''),
    created_at: new FormControl(''),
    created_by: new FormControl(''),
  });
  deleteVacancyForm = new FormGroup({
    id: new FormControl(''),
    title: new FormControl(''),
  });

  constructor(public modalService: NgbModal, private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getAllVacancies().subscribe(data1 => {
      this.vacancies = data1;
      this.dataService.getGlobalUser.subscribe(data2 => {
        this.user = data2;
        this.filtersLoaded = Promise.resolve(true);
        this.dataService.filtersLoaded.next(true);
      });
    });
  }

  ngOnDestroy(): void {
    this.dataService.filtersLoaded.next(false);
  }

  openEditModal(targetModal: any, vacancy: Vacancy): void {
    this.editVacancyForm.patchValue({
      id: vacancy.id,
      title: vacancy.title,
      content: vacancy.content,
      created_at: vacancy.created_at,
      created_by: vacancy.created_by,
    });
    this.modalService.open(targetModal, {size: 'lg'});
  }

  openDeleteModal(targetModal: any, vacancy: Vacancy): void {
    this.deleteVacancyForm.patchValue({
      id: vacancy.id,
      title: vacancy.title,
    });
    this.modalService.open(targetModal);
  }

  onSubmitAdd(): void {
    this.dataService.sendRequestAddVacancy(this.addVacancyForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitEdit(): void {
    this.dataService.sendRequestEditVacancy(this.editVacancyForm).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }

  onSubmitDelete(): void {
    this.dataService.sendRequestDeleteVacancy(this.deleteVacancyForm.get('id')?.value).subscribe(() => {
      this.modalService.dismissAll();
      location.reload();
    });
  }
}
