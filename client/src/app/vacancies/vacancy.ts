export class Vacancy {
  id: number | undefined;
  title: string | undefined;
  content: string | undefined;
  created_at: Date | undefined;
  created_by: string | undefined;
}
