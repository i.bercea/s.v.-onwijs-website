from wtforms import Form, SelectField, PasswordField, TextAreaField, StringField
from wtforms.fields.html5 import EmailField, DateField, DecimalField, DateTimeField
from wtforms.validators import DataRequired, Email, Regexp, Optional


class LoginForm(Form):
    email = EmailField(validators=[DataRequired(), Email()])
    password = PasswordField(validators=[DataRequired()])


class ChangePasswordForm(Form):
    password = PasswordField(validators=[DataRequired()])
    new_password = PasswordField(validators=[DataRequired()])
    confirm = PasswordField(validators=[DataRequired()])

    def validate(self, extra_validators=None):
        if not Form.validate(self):
            return False
        return self.new_password.__eq__(self.confirm)


class ResetPasswordForm(Form):
    password = PasswordField(validators=[DataRequired()])
    token = StringField(validators=[DataRequired()])


class UserForm(Form):
    email = EmailField(validators=[DataRequired(), Email()])
    first_name = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$')])
    initials = StringField(validators=[DataRequired(), Regexp(r'^([A-Z]\.)+$')])
    last_name = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$')])
    date_of_birth = DateField(validators=[DataRequired()])
    main_study = StringField(validators=[DataRequired(), Regexp(r'^[A-Z]{2,3}$')])
    student_number = StringField(validators=[DataRequired(), Regexp(r'^[s][0-9]{1,10}$')])


class AdminForm(UserForm):
    phone_number = StringField(validators=[DataRequired(), Regexp('^[+]?[0-9]{3}[ \-.]?[0-9]{3}[ \-.]?[0-9]{4,6}$')])
    address = StringField(validators=[DataRequired(), Regexp(
        '^(.*?)\s+(\d+[a-zA-Z]{0,1}\s{0,1}[-]{1}\s{0,1}\d*[a-zA-Z]{0,1}|\d+[a-zA-Z-]{0,1}\d*[a-zA-Z]{0,1})$')])
    postal_code = StringField(validators=[DataRequired(), Regexp(r'^[0-9]{4}[A-Z]{2}$')])
    city = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z\- ]+$')])
    iban = StringField(validators=[DataRequired(), Regexp(r'^[A-Z]{2}[0-9]{2}[A-Z]{4}[0-9]{8,30}$')])
    teaching_level = SelectField(choices=['minor', 'master', 'external'], validators=[DataRequired()])
    teaching_area = StringField(validators=[DataRequired()])
    committee = StringField(validators=[Optional()])
    member_since = DateField(validators=[DataRequired()])
    payment_auth_membership = SelectField(choices=['yes', 'no'], validators=[DataRequired()])
    payment_auth_activities = SelectField(choices=['yes', 'no'], validators=[DataRequired()])
    payment_characteristic = StringField(validators=[DataRequired()])
    comments = StringField(validators=[Optional()])
    contribution = DecimalField(validators=[DataRequired()])
    mandate = StringField(validators=[DataRequired()])
    used_for = StringField(validators=[DataRequired()])
    role = SelectField(choices=['admin', 'member'], validators=[DataRequired()])


class AnnouncementForm(Form):
    title = StringField(validators=[DataRequired()])
    content = TextAreaField(validators=[DataRequired()])
    created_at = DateField(validators=[DataRequired()])


class EventForm(Form):
    title = StringField(validators=[DataRequired()])
    start = DateTimeField(validators=[DataRequired()])
    end = DateTimeField(validators=[Optional()])
    allDay = StringField(validators=[DataRequired()])
    backgroundColor = StringField(validators=[DataRequired()])


class VacancyForm(Form):
    title = StringField(validators=[DataRequired()])
    content = TextAreaField(validators=[DataRequired()])
    created_at = DateField(validators=[DataRequired()])


class ActivityForm(Form):
    title = StringField(validators=[DataRequired()])
    content = TextAreaField(validators=[DataRequired()])
    created_at = DateField(validators=[DataRequired()])


class ContactForm(Form):
    name = StringField(validators=[DataRequired()])
    email = EmailField(validators=[DataRequired(), Email()])
    message = TextAreaField(validators=[DataRequired()])


class SignUpForm(Form):
    first_name = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$')])
    initials = StringField(validators=[DataRequired(), Regexp(r'^([A-Z]\.)+$')])
    last_name = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$')])
    date_of_birth = DateField(validators=[DataRequired()])
    main_study = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z]+( [a-zA-Z]+)*$')])
    startYear = StringField(validators=[DataRequired(), Regexp(r'^[1-9][0-9]{3}$')])
    student_number = StringField(validators=[DataRequired(), Regexp(r'^[s][0-9]{1,10}$')])
    street_address = StringField(validators=[DataRequired(), Regexp(
        '^(.*?)\s+(\d+[a-zA-Z]{0,1}\s{0,1}[-]{1}\s{0,1}\d*[a-zA-Z]{0,1}|\d+[a-zA-Z-]{0,1}\d*[a-zA-Z]{0,1})$')])
    city = StringField(validators=[DataRequired(), Regexp(r'^[a-zA-Z\- ]+$')])
    postal_code = StringField(validators=[DataRequired(), Regexp(r'^[0-9]{4}[A-Z]{2}$')])
    phone_number = StringField(validators=[DataRequired(), Regexp('^[+]?[0-9]{3}[ \-.]?[0-9]{3}[ \-.]?[0-9]{4,6}$')])
    email = EmailField(validators=[DataRequired(), Email()])
    iban = StringField(validators=[DataRequired(), Regexp(r'^[A-Z]{2}[0-9]{2}[A-Z]{4}[0-9]{8,30}$')])
    payment_membership = SelectField(choices=['yes', 'no'], validators=[DataRequired()])
    payment_activities = SelectField(choices=['yes', 'no'], validators=[DataRequired()])
    contribution = DecimalField(validators=[DataRequired()])
    program = StringField(validators=[Optional()])
