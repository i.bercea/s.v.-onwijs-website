from flask import Blueprint, make_response, jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import db
from .forms import EventForm
from .models import Event, EventSchema, User

event = Blueprint('event', __name__)


@event.route('/events', methods=['GET'])
@jwt_required(optional=True)
def get_events():
    return make_response(jsonify(EventSchema(many=True).dump(Event.query.all())), 200)


@event.route('/events/add', methods=['POST'])
@jwt_required()
def add_event():
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = EventForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.add(Event(
        title=form.title.data,
        start=form.start.data,
        end=form.end.data,
        allDay=1 if form.allDay.data == '1' else 0,
        backgroundColor=form.backgroundColor.data, ))
    db.session.commit()
    return make_response(jsonify({'message': 'Event added successfully'}), 201)


@event.route('/events/edit/<ID>', methods=['POST'])
@jwt_required()
def edit_event(ID):
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = EventForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(Event).filter_by(id=ID).update({
        'title': form.title.data,
        'start': form.start.data,
        'end': form.end.data,
        'allDay': 1 if form.allDay.data == '1' else 0,
        'backgroundColor': form.backgroundColor.data,
    })
    db.session.commit()
    return make_response(jsonify({'message': 'Event updated successfully'}), 200)


@event.route('/events/delete/<ID>', methods=['DELETE'])
@jwt_required()
def delete_event(ID):
    _event = Event.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _event:
        return make_response(jsonify({'message': f'Event with id={ID} doesn\'t exist'}), 401)
    db.session.delete(_event)
    db.session.commit()
    return make_response(jsonify({'message': 'Event deleted successfully'}), 200)
