import os
import io
import shutil
from PIL import Image
from base64 import encodebytes

from flask import Blueprint, make_response, jsonify, request
from flask_cors import cross_origin
from flask_jwt_extended import jwt_required, get_jwt_identity
from werkzeug.utils import secure_filename

from . import db
from .forms import ActivityForm
from .models import User, Activity, ActivitySchema

activity = Blueprint('activity', __name__)


@activity.route('/activities/<committee>', methods=['GET'])
@jwt_required(optional=True)
def get_activities(committee):
    return make_response(jsonify(ActivitySchema(many=True).dump(
        Activity.query.filter(Activity.committee.contains(committee)).all())), 200)


@activity.route('/activities/images/<committee>/<ID>', methods=['GET', 'POST', 'DELETE'])
@jwt_required(optional=True)
@cross_origin()
def images(committee, ID):
    if request.method == 'POST':
        if not os.path.exists(f'static/images/{committee}/{ID}'):
            os.makedirs(f'static/images/{committee}/{ID}')
        for f in request.files.getlist('file'):
            f.save(os.path.join(f'static/images/{committee}/{ID}/' + str(secure_filename(f.filename))))
        return make_response(jsonify({'message': 'Images added successfully'}), 200)
    elif request.method == 'GET':
        encoded_images = []
        for image in os.listdir(f'static/images/{committee}/{ID}'):
            pil_img = Image.open(f'static/images/{committee}/{ID}/'+image, mode='r')
            byte_arr = io.BytesIO()
            pil_img.save(byte_arr, format='PNG')
            encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii')
            encoded_images.append(encoded_img)
        return make_response(jsonify({'result': encoded_images}), 200)
    else:
        if os.path.exists(f'static/images/{committee}/{ID}'):
            shutil.rmtree(f'static/images/{committee}/{ID}')
        return make_response(jsonify({'message': 'Images deleted successfully'}), 200)


@activity.route('/activities/<committee>/add', methods=['POST'])
@jwt_required()
def add_activity(committee):
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)

    form = ActivityForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    new_activity = Activity(
        committee=committee,
        title=form.title.data,
        content=form.content.data,
        created_at=form.created_at.data)
    db.session.add(new_activity)
    db.session.commit()
    return make_response(jsonify({'id': new_activity.id}), 201)


@activity.route('/activities/edit/<ID>', methods=['POST'])
@jwt_required()
def edit_activity(ID):
    _activity = Activity.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin() and _activity.committee not in current_user.committee.split(','):
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = ActivityForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(Activity).filter_by(id=ID).update({
        'title': form.title.data,
        'content': form.content.data
    })
    db.session.commit()
    return make_response(jsonify({'message': 'Activity updated successfully'}), 200)


@activity.route('/activities/delete/<ID>', methods=['DELETE'])
@jwt_required()
def delete_activity(ID):
    _activity = Activity.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin() and _activity.committee not in current_user.committee.split(','):
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _activity:
        return make_response(jsonify({'message': f'Activity with id={ID} doesn\'t exist'}), 401)
    db.session.delete(_activity)
    db.session.commit()
    return make_response(jsonify({'message': 'Activity deleted successfully'}), 200)
