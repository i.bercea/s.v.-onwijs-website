import bcrypt
from flask import Blueprint, request, make_response, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity, create_access_token, get_jwt, decode_token

from . import limiter, jwt, db, blocklist, app
from .forms import LoginForm, ChangePasswordForm, ResetPasswordForm
from .models import User
from .utils import send_token

auth = Blueprint('auth', __name__)


@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(_, jwt_payload):
    return blocklist.get(jwt_payload['jti']) is not None


@auth.route('/login', methods=['POST'])
@limiter.limit("1000 per minute")  # real time field validation in typescript brakes it
@jwt_required(optional=True)
def login():
    if get_jwt_identity():
        return make_response(jsonify({'message': 'User already logged in'}), 401)
    form = LoginForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    user = User.query.filter_by(email=form.email.data).first()
    if user and bcrypt.checkpw(form.password.data.encode('utf-8'), user.password):
        access_token = create_access_token(identity=user.id)
        return make_response(jsonify({'access_token': access_token}), 200)
    return make_response(jsonify({'message': 'Wrong credentials'}), 401)


@auth.route('/logout', methods=['DELETE'])
@jwt_required()
def logout():
    blocklist.set(get_jwt()['jti'], value='', ex=app.config.get('JWT_ACCESS_TOKEN_EXPIRES'))
    return make_response(jsonify({'message': 'User logged out successfully (1)'}), 200)


@auth.route('/change-password', methods=['POST'])
@jwt_required()
def change_password():
    user = User.query.filter_by(id=get_jwt_identity()).first()
    form = ChangePasswordForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    if not bcrypt.checkpw(form.password.data.encode('utf-8'), user.password):
        return make_response(jsonify({'message': 'Wrong credentials'}), 401)
    db.session.query(User).filter_by(id=get_jwt_identity()).update(
        {'password': bcrypt.hashpw(form.new_password.data.encode('utf-8'), bcrypt.gensalt())})
    db.session.commit()
    blocklist.set(get_jwt()['jti'], value='', ex=app.config.get('JWT_ACCESS_TOKEN_EXPIRES'))
    access_token = create_access_token(identity=get_jwt_identity())
    return make_response(jsonify({'access_token': access_token}), 200)


@auth.route('/reset-password', methods=['POST'])
def reset_password():
    user = User.query.filter_by(email=request.form.get('email')).first()
    if not user:
        return make_response(jsonify({'message': 'Email doesn\'t link to existing account'}), 400)
    send_token(user, create_access_token(user.id))  # request.host_url + 'reset-password/' + token
    return make_response(jsonify({'message': f'Reset token sent successfully to: {user.email}'}), 200)


@auth.route('/reset-password-confirmed', methods=['POST'])
def reset_password_confirmed():
    form = ResetPasswordForm(request.form)
    user = User.query.filter_by(id=decode_token(form.token.data).get('sub')).first()
    if not user:
        return make_response(jsonify({'message': 'Invalid reset token'}), 400)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(User).filter_by(id=user.id).update(
        {'password': bcrypt.hashpw(form.password.data.encode('utf-8'), bcrypt.gensalt())})
    db.session.commit()
    return make_response(jsonify({'message': 'Password successfully reset'}), 200)
