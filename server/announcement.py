from flask import Blueprint, make_response, jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import db
from .forms import AnnouncementForm
from .models import User, Announcement, AnnouncementSchema

announcement = Blueprint('announcement', __name__)


@announcement.route('/announcements', methods=['GET'])
@jwt_required(optional=True)
def get_announcements():
    return make_response(jsonify(AnnouncementSchema(many=True).dump(Announcement.query.all())), 200)


@announcement.route('/announcements/add', methods=['POST'])
@jwt_required()
def add_announcement():
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = AnnouncementForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.add(Announcement(
        title=form.title.data,
        content=form.content.data,
        created_at=form.created_at.data,
        created_by=current_user.full_name))
    db.session.commit()
    return make_response(jsonify({'message': 'Announcement added successfully'}), 201)


@announcement.route('/announcements/edit/<ID>', methods=['POST'])
@jwt_required()
def edit_announcement(ID):
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = AnnouncementForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(Announcement).filter_by(id=ID).update({
        'title': form.title.data,
        'content': form.content.data
    })
    db.session.commit()
    return make_response(jsonify({'message': 'Announcement updated successfully'}), 200)


@announcement.route('/announcements/delete/<ID>', methods=['DELETE'])
@jwt_required()
def delete_announcement(ID):
    _announcement = Announcement.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _announcement:
        return make_response(jsonify({'message': f'Announcement with id={ID} doesn\'t exist'}), 401)
    db.session.delete(_announcement)
    db.session.commit()
    return make_response(jsonify({'message': 'Announcement deleted successfully'}), 200)
