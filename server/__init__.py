import redis
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_pyfile('config.py')

db = SQLAlchemy(app)
ma = Marshmallow(app)
cors = CORS(app)
mail = Mail(app)
jwt = JWTManager(app)
limiter = Limiter(app, key_func=get_remote_address)
blocklist = redis.Redis()

from .auth import auth
from .user import user
from .announcement import announcement
from .event import event
from .vacancy import vacancy
from .activities import activity

app.register_blueprint(auth)
app.register_blueprint(user)
app.register_blueprint(announcement)
app.register_blueprint(event)
app.register_blueprint(vacancy)
app.register_blueprint(activity)

db.create_all()
