import random
from threading import Thread

from flask_mail import Message

from . import mail, app


def generate_password():
    password = ''
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@£$%^&*().,?0123456789'
    for _ in range(10):
        password += random.choice(chars)
    return password


def send_async_email(msg):
    with app.app_context():
        mail.send(msg)


def send_password(user, password):
    msg = Message('Your account\'s password', recipients=[user.email])
    msg.body = f'Use the following password to connect to your account:<br>{password}'
    thr = Thread(target=send_async_email, args=[msg])
    thr.start()
    return thr


def send_token(user, token):
    msg = Message('Token to reset password', recipients=[user.email])
    msg.body = f'Use the following token to reset your password: {token}'
    thr = Thread(target=send_async_email, args=[msg])
    thr.start()
    return thr


def send_contact(form):
    msg = Message('Web app contact form', recipients=[app.config['MAIL_USERNAME']])
    msg.html = f'<p>{form.email.data}&nbsp;({form.name.data})</p><p>{form.message.data}</p>'
    thr = Thread(target=send_async_email, args=[msg])
    thr.start()
    return thr


def send_signup(form):
    msg = Message('Web app sign up form', recipients=[app.config['MAIL_USERNAME']])
    msg.html = f'<table border="1">' \
               f'<tr><th align="left">First&nbsp;name:</th><td>{form.first_name.data}</td></tr>' \
               f'<tr><th align="left">Initials:</th><td>{form.initials.data}</td></tr>' \
               f'<tr><th align="left">Last&nbsp;name:</th><td>{form.last_name.data}</td></tr>' \
               f'<tr><th align="left">Date&nbsp;of&nbsp;birth:</th><td>{form.date_of_birth.data}</td></tr>' \
               f'<tr><th align="left">Study:</th><td>{form.main_study.data}</td></tr>' \
               f'<tr><th align="left">Start&nbsp;year:</th><td>{form.startYear.data}</td></tr>' \
               f'<tr><th align="left">Student&nbsp;number:</th><td>{form.student_number.data}</td></tr>' \
               f'<tr><th align="left">Street&nbsp;address:</th><td>{form.street_address.data}</td></tr>' \
               f'<tr><th align="left">City:</th><td>{form.city.data}</td></tr>' \
               f'<tr><th align="left">Postal&nbsp;code:</th><td>{form.postal_code.data}</td></tr>' \
               f'<tr><th align="left">Phone&nbsp;number:</th><td>{form.phone_number.data}</td></tr>' \
               f'<tr><th align="left">Email:</th><td>{form.email.data}</td></tr>' \
               f'<tr><th align="left">IBAN:</th><td>{form.iban.data}</td></tr>' \
               f'<tr><th align="left">Contribution:</th><td>{form.contribution.data}</td></tr>' \
               f'<tr><th align="left">Program:</th><td>{form.program.data}</td></tr>' \
               f'<tr><th align="left">Membership&nbsp;payment&nbsp;authorization:</th><td>{form.payment_membership.data}</td></tr>' \
               f'<tr><th align="left">Activities&nbsp;payment&nbsp;authorization:</th><td>{form.payment_activities.data}</td></tr>'
    thr = Thread(target=send_async_email, args=[msg])
    thr.start()
    return thr
