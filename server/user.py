import bcrypt
from flask import Blueprint, make_response, jsonify, request
from flask_cors import cross_origin
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import db
from .forms import UserForm, AdminForm, ContactForm, SignUpForm
from .models import User, UserSchema
from .utils import generate_password, send_password, send_contact, send_signup

user = Blueprint('user', __name__)


@user.route('/user', methods=['GET'])
@jwt_required()
def get_user():
    return make_response(
        jsonify(UserSchema(exclude=['password']).dump(User.query.filter_by(id=get_jwt_identity()).first())), 200)


@user.route('/users/<committee>', methods=['GET'])
@jwt_required(optional=True)
def get_users_by_committee(committee):
    return make_response(
        jsonify(UserSchema(many=True, exclude=['password']).dump(
            User.query.filter(User.committee.contains([committee])).all())), 200)


@user.route('/users', methods=['GET'])
@jwt_required()
def get_users():
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    return make_response(jsonify(UserSchema(many=True, exclude=['password']).dump(User.query.all())), 200)


@user.route('/users/add', methods=['POST'])
@jwt_required()
def add_user():
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = AdminForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    passwrd = generate_password()
    new_user = User(email=form.email.data,
                    first_name=form.first_name.data,
                    initials=form.initials.data,
                    last_name=form.last_name.data,
                    full_name=f'{form.initials.data} {form.last_name.data}',
                    date_of_birth=form.date_of_birth.data,
                    main_study=form.main_study.data,
                    student_number=form.student_number.data,
                    phone_number=form.phone_number.data,
                    address=form.address.data,
                    postal_code=form.postal_code.data,
                    city=form.city.data,
                    iban=form.iban.data,
                    teaching_level=form.teaching_level.data,
                    teaching_area=form.teaching_area.data,
                    committee=form.committee.data,
                    member_since=form.member_since.data,
                    payment_auth_membership=form.payment_auth_membership.data,
                    payment_auth_activities=form.payment_auth_activities.data,
                    payment_characteristic=form.payment_characteristic.data,
                    comments=form.comments.data,
                    contribution=form.contribution.data,
                    mandate=form.mandate.data,
                    used_for=form.used_for.data,
                    role=form.role.data,
                    password=bcrypt.hashpw(passwrd.encode('utf-8'), bcrypt.gensalt()))
    db.session.add(new_user)
    db.session.commit()
    send_password(new_user, passwrd)
    return make_response(jsonify({'message': 'User added successfully'}), 201)


@user.route('/users/edit', methods=['POST'])
@cross_origin()
@jwt_required()
def edit_user():
    form = UserForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(User).filter_by(id=get_jwt_identity()).update({
        'email': form.email.data,
        'first_name': form.first_name.data,
        'initials': form.initials.data,
        'last_name': form.last_name.data,
        'full_name': f'{form.initials.data} {form.last_name.data}',
        'date_of_birth': form.date_of_birth.data,
        'main_study': form.main_study.data,
        'student_number': form.student_number.data})
    db.session.commit()
    return make_response(jsonify({'message': 'User updated successfully (1)'}), 200)


@user.route('/users/edit/<ID>', methods=['POST'])
@jwt_required()
def edit_admin(ID):
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    _user = User.query.filter_by(id=ID).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _user:
        return make_response(jsonify({'message': f'User with id={ID} doesn\'t exist'}), 401)
    form = AdminForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(User).filter_by(id=ID).update(
        {'email': form.email.data,
         'first_name': form.first_name.data,
         'initials': form.initials.data,
         'last_name': form.last_name.data,
         'full_name': f'{form.initials.data} {form.last_name.data}',
         'date_of_birth': form.date_of_birth.data,
         'main_study': form.main_study.data,
         'student_number': form.student_number.data,
         'phone_number': form.phone_number.data,
         'address': form.address.data,
         'postal_code': form.postal_code.data,
         'city': form.city.data,
         'iban': form.iban.data,
         'teaching_level': form.teaching_level.data,
         'teaching_area': form.teaching_area.data,
         'committee': form.committee.data,
         'member_since': form.member_since.data,
         'payment_auth_membership': form.payment_auth_membership.data,
         'payment_auth_activities': form.payment_auth_activities.data,
         'payment_characteristic': form.payment_characteristic.data,
         'comments': form.comments.data,
         'contribution': form.contribution.data,
         'mandate': form.mandate.data,
         'used_for': form.used_for.data,
         'role': form.role.data})
    db.session.commit()
    return make_response(jsonify({'message': 'User updated successfully (2)'}), 200)


@user.route('/users/delete/<ID>', methods=['DELETE'])
@jwt_required()
def delete_user(ID):
    _user = User.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _user:
        return make_response(jsonify({'message': f'User with id={ID} doesn\'t exist'}), 401)
    if _user == current_user:
        return make_response(jsonify({'message': 'You can\'t delete your own account'}), 401)
    db.session.delete(_user)
    db.session.commit()
    return make_response(jsonify({'message': 'User deleted successfully'}), 200)


@user.route('/contact', methods=['POST'])
@jwt_required(optional=True)
def contact():
    form = ContactForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    send_contact(form)
    return make_response(jsonify({'message': 'Form sent successfully'}), 200)


@user.route('/signup', methods=['POST'])
@jwt_required(optional=True)
def signup():
    form = SignUpForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    send_signup(form)
    return make_response(jsonify({'message': 'Form sent successfully'}), 200)
