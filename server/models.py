from datetime import date

from . import db, ma


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(100), unique=True, nullable=False)
    first_name = db.Column(db.String(100), nullable=False)
    initials = db.Column(db.String(10), nullable=False)
    last_name = db.Column(db.String(100), nullable=False)
    full_name = db.Column(db.String(111), nullable=False)
    date_of_birth = db.Column(db.Date, nullable=False)
    main_study = db.Column(db.Enum('TCS', 'BIT'), nullable=False)
    student_number = db.Column(db.String(10), unique=True, nullable=False)
    phone_number = db.Column(db.String(18), nullable=False)
    address = db.Column(db.String(100), nullable=False)
    postal_code = db.Column(db.String(6), nullable=False)
    city = db.Column(db.String(20), nullable=False)
    iban = db.Column(db.String(38), nullable=False)
    teaching_level = db.Column(db.String(20), nullable=False)
    teaching_area = db.Column(db.String(50), nullable=False)
    committee = db.Column(db.String(255), nullable=True)
    member_since = db.Column(db.Date, nullable=False, default=date.today())
    payment_auth_membership = db.Column(db.Enum('yes', 'no'), nullable=False)
    payment_auth_activities = db.Column(db.Enum('yes', 'no'), nullable=False)
    payment_characteristic = db.Column(db.String(10), nullable=False)
    comments = db.Column(db.String(255), unique=False)
    contribution = db.Column(db.DECIMAL(6, 2), nullable=False)
    mandate = db.Column(db.String(20), nullable=False)
    used_for = db.Column(db.String(10), nullable=False, default='RCUR')
    role = db.Column(db.Enum('admin', 'member'), nullable=False)
    password = db.Column(db.Binary(60), nullable=False)

    def is_admin(self):
        return self.role == 'admin'

    def get_name(self):
        return self.full_name


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        # exclude = ('password')


class Announcement(db.Model):
    __tablename__ = "announcements"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    content = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.Date, nullable=False)
    created_by = db.Column(db.String(255), nullable=False)


class AnnouncementSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Announcement


class Event(db.Model):
    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    start = db.Column(db.DateTime, nullable=False)
    end = db.Column(db.DateTime, nullable=True)
    allDay = db.Column(db.Boolean, nullable=False)
    backgroundColor = db.Column(db.String(7), nullable=False)


class EventSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Event


class Vacancy(db.Model):
    __tablename__ = "vacancies"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(255), nullable=False)
    content = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.Date, nullable=False)
    created_by = db.Column(db.String(255), nullable=False)


class VacancySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Vacancy


class Activity(db.Model):
    __tablename__ = "activities"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    committee = db.Column(db.Enum('KFC', 'OLC', 'Kascommissie', 'Raad Van Advies', 'OproepCie'), nullable=False)
    title = db.Column(db.String(255), nullable=False)
    content = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.Date, nullable=False)


class ActivitySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Activity
