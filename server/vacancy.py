from flask import Blueprint, make_response, jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity

from . import db
from .forms import VacancyForm
from .models import User, Vacancy, VacancySchema

vacancy = Blueprint('vacancy', __name__)


@vacancy.route('/vacancies', methods=['GET'])
@jwt_required(optional=True)
def get_vacancies():
    return make_response(jsonify(VacancySchema(many=True).dump(Vacancy.query.all())), 200)


@vacancy.route('/vacancies/add', methods=['POST'])
@jwt_required()
def add_vacancy():
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = VacancyForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.add(Vacancy(
        title=form.title.data,
        content=form.content.data,
        created_at=form.created_at.data,
        created_by=current_user.full_name))
    db.session.commit()
    return make_response(jsonify({'message': 'Vacancy added successfully'}), 201)


@vacancy.route('/vacancies/edit/<ID>', methods=['POST'])
@jwt_required()
def edit_vacancy(ID):
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    form = VacancyForm(request.form)
    if not form.validate():
        return make_response(jsonify({'message': 'Form invalidated'}), 400)
    db.session.query(Vacancy).filter_by(id=ID).update({
        'title': form.title.data,
        'content': form.content.data
    })
    db.session.commit()
    return make_response(jsonify({'message': 'Vacancy updated successfully'}), 200)


@vacancy.route('/vacancies/delete/<ID>', methods=['DELETE'])
@jwt_required()
def delete_vacancy(ID):
    _vacancy = Vacancy.query.filter_by(id=ID).first()
    current_user = User.query.filter_by(id=get_jwt_identity()).first()
    if not current_user.is_admin():
        return make_response(jsonify({'message': 'You don\'t have permission'}), 401)
    if not _vacancy:
        return make_response(jsonify({'message': f'Vacancy with id={ID} doesn\'t exist'}), 401)
    db.session.delete(_vacancy)
    db.session.commit()
    return make_response(jsonify({'message': 'Vacancy deleted successfully'}), 200)
